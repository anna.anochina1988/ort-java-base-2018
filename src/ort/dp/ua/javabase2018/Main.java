package ort.dp.ua.javabase2018;

  import java.util.Scanner;

   public class Main {
	   
	    public static void main (String [] args){
		
		   //������� ����� �� 0 �� 8
		   final int doskaMin = 0;
		   final int doskaMax = 8;
		
		   //���������� ����� �� ����� - ���������� �����
		   int  ay = 5;		
		   int bx = 7;
		
		   //���� �d (����, �������� �������� �����) - ����
		   final int cy = 4;
		   final int dx = 3;
		
           boolean danger = (bx >= doskaMin && bx <= dx) && (ay>= doskaMin && ay <= doskaMax)||
        		          (bx >= cy && bx <= doskaMax) && (ay >= doskaMin && ay <= cy)||
        		          (ay >= doskaMin && ay <= cy) && (bx >= doskaMin && bx <= doskaMax) ||
        		          (ay == ay && ay <= doskaMax) && (bx >= doskaMin && bx <= dx);
         
           System.out.println ("����� � (�����) �����" + " " + danger);
		
		   //������ � ���� - ���� � �������! true - �� ���������, false ��������
		   //������������� ����� dx1= 3 � cy1 =4. ������ ������! ����� ������ ay � bx
		   int ay1 = 2;
		   int bx1 = 6;
		   final int cy1 = 4;//��� ����������� �����.
		   final int dx1 = 3;//
	
		
		   //������� ��� bx � ay ����� ��� ��� ���� ������ �������� ���� dx!!!
		
		   boolean danger1 = bx1 == doskaMin && (ay1 >= doskaMin && ay1 <= 7) ||
				          bx1 == 1 && (ay1 >= doskaMin && ay1 <= 5) ||
				          bx1 == 2 && (ay1 >= doskaMin && ay1 <= 5) ||
				          bx1 == 3 && (ay1 >= doskaMin && ay1 <= 7) ||
				          bx1 == 4 && (ay1 >= 1 && ay1 <= doskaMax) ||
				          bx1 == 5 && (ay1 >= 2 && ay1 <= doskaMax) ||
				          bx1 == 6 && (ay1 >= 3 && ay1 <= doskaMax) ||
				          bx1 == 7 && (ay1 >= 4 && ay1 <= doskaMax) ||
				          bx1 == doskaMax && (ay1 >= 5 && ay1 <= doskaMax) ||
				          ay1 == doskaMin &&  (bx1 >= doskaMin && bx1 <= 7) ||
				          ay1 == 1 && (bx1 >= doskaMin && bx1 <= 6) ||
				          ay1 == 2 && (bx1 >= doskaMin && bx1 <= 6) ||
				          ay1 == 3 && (bx1 >= doskaMin && bx1 <= 4) ||
				          ay1 == 4 && (bx1 >= doskaMin && bx1 <=7) ||
				          ay1 == 5 && (bx1 >= doskaMin && bx1 <=doskaMax) ||
				          ay1 == 6 && (bx1 >= 2 && bx1 <= doskaMax) ||
				          ay1 == 7 && (bx1 >= 3 && bx1 <= doskaMax) ||
				          ay1 == doskaMax && (bx1 >= 4 && bx1 <=doskaMax);
		  System.out.println ("����� � (����) �����" + " " + danger1);
		
		
		 //�) �� ���� (a, b) ���������� ������. �������� �������, ��� ������� �� �����
		 //����� ����� ������� �� ���� (c, d).
			
		 int doska2 = 8;
		 int ay2 = 1;
		 int bx2 = 4;
		 final int cy2 = 4;
		 final int dx2 = 3;
		
		 //final int cy2 = 4; ��� ����������� ����� 
		 //final int dx2 = 3;
		
	     boolean danger2 = (bx2 >= dx2 && bx2 <= 4) && (ay2 >= 0 && ay2 <= 5)||
			              (ay2 >= 4 && ay2 <= 5) && (bx2 >= 0 && bx2 <= 4);
	     System.out.println ("����� � (������) �����" + " " + danger2);
	 
	     //�) �� ���� (a, b) ���������� �����. �������� �������, ��� ������� �� ////������-
	     //�� ���� (c, d).
	     int ay3 = 5;
	     int bx3 = 7;
	    
	     //dx = 3; cy = 4; �� ����� �������� ���! � ������� �� ������ ����� �� ����� �-�� ������
	     boolean danger3 = (bx3 >= doskaMin && bx3 <= doskaMax) && (ay3 >= doskaMin && ay3 <= doskaMax);
	     System.out.println ("����� � (�����) �����" + " " + danger3);
	
	
	    //�) �� ���� (a, b) ����������� ����� �����. �������� �������, ��� �������
	    //��� ����� ����� ����� ������� �� ���� (c, d):
	    //��� ������� ����;
	    //����� ��� "����" ������ ��� ����� ���������.
	    //����������
	    //����� ����� ������������ �� ����� ����� �����.
	    int ay4 = 5;
	    int bx4 = 7;
	    final int cy4 = 4;
	    final int dx4 = 3;
	     
	    // �-1 ������� ��� ����� �����
	    boolean danger4 = bx4 <= dx4 && ay4 <= cy4; //������� ��� ����� ����� (�-1)
	    System.out.println ("����� � (����� ����� ������� ���) �����" + " " + danger4);
	    
	    //�-2 ����� ����� ����� ���� 
	    int ay5 = 5;
	    int bx5 = 7;
	    final int cy5 = 4;
	    final int dx5 = 3;
	
	    boolean danger5 = (bx5 >= doskaMin && bx5 <= dx5) && (ay5 >= doskaMin && ay5 <= (cy5 / 2));
	    System.out.println ("����� �-2 (����� ����� ����) �����" + " " + danger5);
	    

	    //�) �� ���� (a, b) ����������� ������ �����. �������� �������, ��� �������
	    //��� ����� ����� ����� ������� �� ���� (c, d):
	    //��� ������� ����;
	    //����� ��� "����" ������ ��� ����� ���������.
	    int ay6 = 3;
	    int bx6 = 4;
	    final int cy7 = 4;
	    final int dx7 = 3;
	    
	    boolean danger6 = (bx6 >= doskaMin && bx6 <= dx7) && (ay6 >= (cy7 - 3) && ay6 <= cy7 + 1);
	    System.out.println ("����� �-1 (������. ������� ���) �����" + " " + danger6);
	    
	    //E2 ����� ����� ����� ����
	    boolean danger7 = (bx6 >= doskaMin && bx6 <= dx7) && (ay6 >= (cy7 / 2) && ay6 <= (cy7 + 2));
	    System.out.println ("����� �-2 (����� ������ ����) �����" + " " + danger7);
	    
	    //������� 4,36 - C������� �� �������� 
	    
	    Scanner s = new Scanner (System.in);
	    System.out.println ("������� ����� ����� �� 1 �� 60: ");
		int t = s.nextInt();
		System.out.println (t);
		
	    int b = t%10; //��������� ������� �� ������� ����� t, ������� ���� ������������, �� ����� 10
	    
	    if (t > 60 || t==0 || t < 0 ) {
	    	System.out.println (false + " " + "���� �� ����� ������ �����, �� ��� ��:");
	    	
	    } else if (t >0 && t <= 60) {

            System.out.println ("��� �������! ������ �����:"); 
            }
	    
	    if (b==1 || b==2 || b==3 || b==6 || b==7 || b==8) {
	    	System.out.println ("������� ����");
	    } else if (b==4 || b==5 || b==9 || b==10 || b==0) {
	    	System.out.println ("������� ����");
	    }
	    
	    //������� 4,65 �������� �� ��� ���������� 
	    
	    Scanner k = new Scanner (System.in);
	    System.out.println ("������� ��� � ������� YYYY: ");
		int year = k.nextInt();
		System.out.println (year);

  
         boolean leap = false;

        if(year % 4 == 0)
        {
            if( year % 100 == 0)
            {
                 //������� �� 400. �������� ����������.
                if ( year % 400 == 0)
                    leap = true;
               else
                    leap = false;
            }
            else
                leap = true;
        }
        else
            leap = false;

        if(leap)
            System.out.println(year + " " +" ���������� ���");
        else
            System.out.println(year + " " + "�� ���������� ���");
	    
	    
	    
	   
}
}