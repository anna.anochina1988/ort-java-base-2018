package ort.dp.ua.javabase2018;

public class Arrayalgorithms1 {

	public static void main(String[] args) {

		// Weather for the month
		int[] array = new int[30];

		for (int i = 0; i < array.length; i++) {
			array[i] = (-10) + (int) (Math.random() * ((10 - (-10)) + 1));
			// System.out.println(array[i]);
		}

		// Snow days
		int countsnow = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < 0)
				countsnow++;
		}

		System.out.println("It was snowing:" + " " + countsnow + " " + "days");

		// Rain days
		int countrain = 0;
		for (int j = 0; j < array.length; j++) {
			if (array[j] > 0)
				countrain++;
		}

		System.out.println("It was rain:" + " " + countrain + " " + "days");

		// days when is neither been rain nor snow
		int countno = 0;
		for (int k = 0; k < array.length; k++) {
			if (array[k] == 0)
				countno++;
		}

		System.out.println("not rain, not snow:" + " " + countno + " " + "days");

	}
}
