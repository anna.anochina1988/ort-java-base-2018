package ort.dp.ua.javabase2018;

public class Constructor22 {
	class Coordinatepoints {
		private String name;
		private int[] koordinata;

		public Coordinatepoints(String name) {
			this(name, new int[2]);
		}

		public Coordinatepoints(String name, int[] koordinata) {
			this.name = name;

			this.koordinata = new int[koordinata.length];
			System.arraycopy(koordinata, 0, this.koordinata, 0, koordinata.length);
		}
	}

}