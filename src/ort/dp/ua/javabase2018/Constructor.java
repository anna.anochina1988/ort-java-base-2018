package ort.dp.ua.javabase2018;

public class Constructor {

	public static void main(String[] args) {
		
	
		Coordinatepoints tochkaa = new Coordinatepoints();
		
    	tochkaa.name = "a";
    	tochkaa.koordinata = new int[2];
    	tochkaa.koordinata[0] = 1;
    	tochkaa.koordinata[1] = 1;
    	

    	Coordinatepoints tochkab = new Coordinatepoints();
    	tochkab.name = "b";
    	tochkab.koordinata = new int[2];
    	tochkab.koordinata[0] = 2;
    	tochkab.koordinata[1] = 2;
    	
        Coordinatepoints tochkac = new Coordinatepoints();
        tochkac.name = "c";
        tochkac.koordinata = new int[2];
        tochkac.koordinata[0] = 3;
        tochkac.koordinata[1] = 3;
    	
        Coordinatepoints [] coordinatepoints1 = {tochkaa, tochkab , tochkac};
        sortByAvgMark(coordinatepoints1);
        printStudents(coordinatepoints1);
    }

    static void printStudents(Coordinatepoints[] coordinatepoints1) {
        for (int i = 0; i < coordinatepoints1.length; i++) {
            System.out.printf(" %s%n", coordinatepoints1[i].name);
        }
    }

    static void sortByAvgMark(Coordinatepoints[] coordinatepoints1) {
        for (int i = coordinatepoints1.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (avg(coordinatepoints1[j]) < avg(coordinatepoints1[j+1])) {
                	Coordinatepoints temp = coordinatepoints1[j];
                	coordinatepoints1[j] = coordinatepoints1[j+1];
                	coordinatepoints1[j+1] = temp;
                }
            }
        }
    }

    static double avg(Coordinatepoints coordinatepoints1) {
        int sum = 0;
        for (int i = 0; i < coordinatepoints1.koordinata.length; i++) {
            sum += coordinatepoints1.koordinata[i];
        }

        return (double) sum / coordinatepoints1.koordinata.length;
    }

    
}

class Coordinatepoints {
    String name;
    int[] koordinata;
}

