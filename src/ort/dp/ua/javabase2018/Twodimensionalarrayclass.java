package ort.dp.ua.javabase2018;

public class Twodimensionalarrayclass {

	public static void main(String[] args) {
		// Array declaration
		int[][] a = new int[3][3];

		// What numbers will be filled two-dimensional array.
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = (int) (Math.random() * 10);
			}
		}

		// 1.the first for i loop iterates through each line of
		// a two-dimensional array
		// 2.second for loop j iterates over columns in this row
		for (int i = 0; i < a.length; i++, System.out.println()) {
			for (int j = 0; j < a[i].length; j++) {
				System.out.print(a[i][j] + " ");
			}
		}

		// Sum two dimensional array
		int sum = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				sum += a[i][j];
			}
		}

		System.out.print("Sum of number two dimensional array:" + " " + sum);

	}
}
